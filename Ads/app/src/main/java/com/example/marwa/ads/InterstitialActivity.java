package com.example.marwa.ads;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class InterstitialActivity extends AppCompatActivity {

    private Button mShowButton;
    private InterstitialAd mInterstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interstitial);

        mShowButton = (Button) findViewById(R.id.showButton);
        mShowButton.setEnabled(false);

    }

    public void loadInterstitial(View view) {
        mShowButton.setEnabled(false);
        mShowButton.setText("Loading Interstatial");

        mInterstitial = new InterstitialAd(this);
        mInterstitial.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitial.setAdListener(new ToastListener(this){
            @Override
            public void onAdLoaded() {
                super.onAdLoaded(); // We call our parent method that will display the Toast message
                mShowButton.setText("Show Interstatial");
                mShowButton.setEnabled(true);
            }
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode); //We call our parent method that will display the Toast message
                mShowButton.setText(getErrorReason());
            }
        });

        AdRequest ad = new AdRequest.Builder().build();
        mInterstitial.loadAd(ad);
    }

    public void showInterstitial(View view) {
        if(mInterstitial.isLoaded()){
            mInterstitial.show();
        }


        mShowButton.setText("Interstitial Not Ready");
        mShowButton.setEnabled(false);

    }

}
