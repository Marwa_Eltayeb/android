package com.example.marwa.birthdate;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    EditText txtBDate;
    TextView txtAge;
    Button btnCalc;
    VideoView video;
    int rQ = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtBDate = (EditText) findViewById(R.id.txtBDate);
        txtAge = (TextView) findViewById(R.id.txtAge);
        btnCalc = (Button) findViewById(R.id.btnCalc);
        video = (VideoView) findViewById(R.id.videoView);

        btnCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
                int YearNow = Integer.parseInt(sdf.format(new Date()));
                int YearOfBirth = Integer.parseInt(txtBDate.getText().toString());
                int age = YearNow - YearOfBirth;
                txtAge.setText(age+"");
            }
        });



    }

    // Make Video
    public void makeVideo(View view) {
        Intent takeVideo = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideo.resolveActivity(getPackageManager()) != null){
            startActivityForResult(takeVideo, rQ);
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == rQ && resultCode == RESULT_OK){
            Uri videoUri = data.getData();
            video.setVideoURI(videoUri);
            video.setMediaController(new MediaController(this));
            video.requestFocus();
            video.start();
        }
    }
}
